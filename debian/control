Source: django-websocket-redis
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Michael Fladischer <fladi@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-setuptools
Standards-Version: 3.9.8
Homepage: https://github.com/jrief/django-websocket-redis
Vcs-Browser: https://salsa.debian.org/python-team/packages/django-websocket-redis
Vcs-Git: https://salsa.debian.org/python-team/packages/django-websocket-redis.git

Package: python3-django-websocket-redis
Architecture: all
Depends: python3-django,
         ${misc:Depends},
         ${python3:Depends}
Recommends: uwsgi-plugin-python3
Description: Websockets for Django applications using Redis (Python3 version)
 This module implements websockets on top of Django without requiring any
 additional framework. For messaging it uses the Redis datastore. In a
 production environment, it is intended to work under uWSGI and behind NGiNX or
 Apache. In a development environment, it can be used with the Django built-in
 webserver.
 .
 This package contains the Python 3 version of the library.
